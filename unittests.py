import unittest
from Cuenta import Cuenta
from Cuenta import Programa

class TestCuenta(unittest.TestCase):

#    def testnumero(self):
 #       self.assertRaises(Cuenta(0, 2000, 200))

    def testIngreso(self):
       #programa(t, num, d, c)
       i=Cuenta(98346537, 2000)
      

       self.assertEqual(i.get_dinero(), 2000)

    def testReintegro(self):
        r=Cuenta(12345678, 2000)
        r.Ingreso(200)
        self.assertEqual(r.get_dinero(), 2200)
   
if __name__ == "__main__":
    unittest.main()
